FROM python:3.7.3-alpine3.9

ENV PYTHONUNBUFFERED 1

EXPOSE 8080

RUN mkdir /code

WORKDIR /code

COPY . /code/
COPY run.sh ./

RUN pip install -r requirements.txt

CMD ["./run.sh"]
